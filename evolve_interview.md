

```python
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
import seaborn
import datetime as dt

import nltk
from nltk.stem import SnowballStemmer

import sklearn
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.cluster import KMeans

import scipy
import scipy.interpolate as sc_int

from pprint import pprint

from time import time

from IPython.display import display
```


```python
%matplotlib inline
```

# Evolve Interview Project

## Zoë Farmer

# Who am I?

* My name is Zoë Farmer
* I'm a recent CU graduate with a BS in Applied Math and a CS Minor
* I'm a co-coordinator of the Boulder Python Meetup
* I'm a big fan of open source software
* http://www.dataleek.io
* [@thedataleek](http://www.twitter.com/thedataleek)
* [git(hub|lab).com/thedataleek](http://github.com/thedataleek)

# General Tooling Overview

* Everything is in Python3.6
* I use `pandas`, `numpy`, `matplotlib`, `scikit-learn`, `nltk`, and `scipy`.
* Some code has been skipped for brevity. See [this link](www.gitlab.com/thedataleek/evolve_interview) for full code.

# The Data

# Listings - details about locations

Importing `.csv` data in `pandas` is easy.


```python
listing_data = pd.read_csv('./ListingsAirbnbScrapeExam.csv')
```


```python
', '.join(listing_data.columns)
```




    'id, name, summary, space, description, experiences_offered, neighborhood_overview, notes, transit, access, interaction, house_rules, host_name, host_since, host_location, host_about, host_response_time, host_response_rate, host_acceptance_rate, host_is_superhost, host_neighbourhood, host_listings_count, host_total_listings_count, host_verifications, host_has_profile_pic, host_identity_verified, street, neighbourhood_cleansed, city, state, zipcode, market, smart_location, country_code, country, latitude, longitude, is_location_exact, property_type, room_type, accommodates, bathrooms, bedrooms, beds, bed_type, amenities, square_feet, price, weekly_price, monthly_price, security_deposit, cleaning_fee, guests_included, extra_people, minimum_nights, maximum_nights, calendar_updated, has_availability, availability_30, availability_60, availability_90, availability_365, calendar_last_scraped, number_of_reviews, first_review, last_review, review_scores_rating, review_scores_accuracy, review_scores_cleanliness, review_scores_checkin, review_scores_communication, review_scores_location, review_scores_value, requires_license, license, jurisdiction_names, instant_bookable, cancellation_policy, require_guest_profile_picture, require_guest_phone_verification, calculated_host_listings_count, reviews_per_month'



# Calendar Data - location occupancy by date

* We want to parse these fields
    * datestrings to be formatted as python `datetime` objects
    * price field to be floats


```python
price_re = '^ *\$([0-9]+\.[0-9]{2}) *$'
def price_converter(s):
    match = re.match(price_re, s)
    if match:
        return float(match[1])
    else:
        return np.nan
```


```python
calendar_data = pd.read_csv(
    './CalendarAirbnbScrapeExam.csv',
    converters={
        'available': lambda x: True if x == 'f' else False,
        'price': price_converter
    }
)
calendar_data['filled'] = ~calendar_data['available']
calendar_data['date'] = pd.to_datetime(calendar_data['date'],
                                       infer_datetime_format=True)
```


```python
calendar_data.head(1)
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>listing_id</th>
      <th>date</th>
      <th>available</th>
      <th>price</th>
      <th>filled</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>12147973</td>
      <td>2017-09-05</td>
      <td>True</td>
      <td>NaN</td>
      <td>False</td>
    </tr>
  </tbody>
</table>
</div>



# Dataset Merge

* We want to combine datasets
* Let's calculate the number of nights occupied per listing and add to the listing data.
* But let's first make sure the datasets overlap.


```python
listing_keys = set(listing_data.id)
calendar_keys = set(calendar_data.listing_id)
difference = listing_keys.difference(calendar_keys)
print(f'# Listing Keys: {len(listing_keys)}')
print(f'# Calendar Keys: {len(calendar_keys)}')
print(f'# Difference: {len(difference)}')
```

    # Listing Keys: 3585
    # Calendar Keys: 2872
    # Difference: 713


They don't, in fact we're missing information on about 700 listings.

For our `num_filled` column let's establish the assumption that a `NaN` value stands for "unknown".

# Groupby

We can simply `sum()` our `available` and `filled` boolean fields.

Note, in the final aggregated sum these two fields sum to 365.


```python
fill_dates = calendar_data.groupby('listing_id')[['available', 'filled']].sum()
display(fill_dates.head())
fill_dates['listing_id'] = fill_dates.index
```


<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>available</th>
      <th>filled</th>
    </tr>
    <tr>
      <th>listing_id</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5506</th>
      <td>21.0</td>
      <td>344.0</td>
    </tr>
    <tr>
      <th>6695</th>
      <td>41.0</td>
      <td>324.0</td>
    </tr>
    <tr>
      <th>6976</th>
      <td>46.0</td>
      <td>319.0</td>
    </tr>
    <tr>
      <th>8792</th>
      <td>117.0</td>
      <td>248.0</td>
    </tr>
    <tr>
      <th>9273</th>
      <td>1.0</td>
      <td>364.0</td>
    </tr>
  </tbody>
</table>
</div>


# Left Join

Now we merge with our original dataset using a left join.


```python
combined_data = listing_data.merge(
    fill_dates,
    how='left',
    left_on='id',
    right_on='listing_id'
)
```

# Seasonal Trends

We have a year of data, let's examine how seasons effect occupancy.

We can take a naive approach and simply `groupby` each date and plot the number of dates filled. 


```python
calendar_data.groupby('date')[['filled']].sum().plot(figsize=(12, 6))
plt.savefig('./evolve_interview/naive_occupancy.png')
```


![png](evolve_interview_files/evolve_interview_21_0.png)


<img src="./evolve_interview/naive_occupancy.png" />

# Let's do better

Let's look at only the listings that are filled each day of the year, and look at their prices as the year goes by.

We'll refer to these as "indicator listings".


```python
days_filled = calendar_data.groupby('listing_id')[['filled']].sum()
top_listings = days_filled[days_filled.filled == 365].index
```


```python
print(f'Original Datasize: {len(calendar_data.listing_id.unique())}.')
print(f'Pruned Datasize: {len(top_listings)}')
```

    Original Datasize: 2872.
    Pruned Datasize: 81


This shrinks our dataset by a lot, but that's ok.

We're looking for indicator listings, not the entire dataset.


```python
pruned_calendar_data = calendar_data[
    calendar_data['listing_id'].isin(top_listings)
]
```

# Plot


```python
plt.figure(figsize=(12, 6))
for lid in top_listings:
    cdata = pruned_calendar_data[pruned_calendar_data.listing_id == lid]
    cdata = cdata.sort_values('date')
    plt.plot(cdata.date, cdata.price)
plt.savefig('./evolve_interview/all_filled.png')
```


![png](evolve_interview_files/evolve_interview_29_0.png)


<img src="./evolve_interview/all_filled.png" />

# Reducing Noise

* Too much noise
* Remove all listings with low standard deviation
    * $10 < \sigma < 200$
* Also cut out all listings that only have a few unique values
    * $\left\lvert \left\{ X \right\}\right\rvert > 10$
* Periodicity is the enemy of seasonal trends


```python
listing_price_deviations = pruned_calendar_data.groupby('listing_id')[['price']].std()
listing_price_deviations.rename(columns={'price': 'stddev'}, inplace=True)
listing_price_deviations = listing_price_deviations[
    np.logical_and(
        listing_price_deviations.stddev > 10,
        listing_price_deviations.stddev < 200
    )
]
listing_periodicity = pruned_calendar_data.groupby('listing_id')[['price']].nunique()
listing_periodicity.rename(columns={'price': 'num_unique'}, inplace=True)
listing_periodicity = listing_periodicity[
    listing_periodicity.num_unique > 10
]
sensitive_listings = listing_price_deviations.join(listing_periodicity, how='inner')
```


```python
sensitive_listings
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>stddev</th>
      <th>num_unique</th>
    </tr>
    <tr>
      <th>listing_id</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5455004</th>
      <td>139.340961</td>
      <td>108</td>
    </tr>
    <tr>
      <th>6119918</th>
      <td>128.657169</td>
      <td>216</td>
    </tr>
    <tr>
      <th>8827268</th>
      <td>155.416406</td>
      <td>189</td>
    </tr>
    <tr>
      <th>14421304</th>
      <td>128.396181</td>
      <td>96</td>
    </tr>
    <tr>
      <th>14421403</th>
      <td>127.944730</td>
      <td>105</td>
    </tr>
    <tr>
      <th>14421692</th>
      <td>127.944730</td>
      <td>105</td>
    </tr>
  </tbody>
</table>
</div>




```python
sensitive_listings = sensitive_listings.index
sensitive_calendar_data = calendar_data[calendar_data['listing_id'].isin(sensitive_listings)]
```

# Plot Indictor Listings


```python
plt.figure(figsize=(12, 6))
for lid in sensitive_listings:
    cdata = sensitive_calendar_data[sensitive_calendar_data.listing_id == lid]
    cdata = cdata.sort_values('date')
    label = combined_data[combined_data.id == lid].name.values[0]
    plt.plot(cdata.date, cdata.price, label=label)
plt.legend(loc=0)
plt.savefig("./evolve_interview/indicators.png")
```


![png](evolve_interview_files/evolve_interview_36_0.png)


<img src="./evolve_interview/indicators.png" />

Now let's compare that back to our total number of bookings by date chart that we made earlier.


```python
fig, axarr = plt.subplots(2, 1, figsize=(16, 12))
for lid in sensitive_listings:
    cdata = sensitive_calendar_data[sensitive_calendar_data.listing_id == lid]
    cdata = cdata.sort_values('date')
    label = combined_data[combined_data.id == lid].name.values[0]
    axarr[0].plot(cdata.date.values, cdata.price.values, label=label)
axarr[0].legend(loc=0)

fill_dates = calendar_data.groupby('date')[['filled']].sum()
axarr[1].plot(fill_dates.index.values, fill_dates.filled.values)

plt.savefig("./evolve_interview/indicators_occupancy.png")
```


![png](evolve_interview_files/evolve_interview_39_0.png)


<img src="./evolve_interview/indicators_occupancy.png" />
